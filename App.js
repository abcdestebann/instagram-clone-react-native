import React from 'react';
import { StyleSheet, View } from 'react-native';
import RoutingNoAuthenticated from './app/config/routes/routes-noauthenticated';
import RoutingAuthenticated from './app/config/routes/routes-authenticated';

import { Provider } from 'react-redux';
import store from './app/store/store';

console.disableYellowBox = ['Remote debugger'];


export default class App extends React.Component {
  constructor() {
    super();
    this.state = {};
  }
  render() {
    return (
      <View style={styles.container}>
        {/* <RoutingNoAuthenticated /> */}
        <Provider store={store}>
          <RoutingNoAuthenticated />
        </Provider>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
