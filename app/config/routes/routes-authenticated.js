// NATIVE
import { TabNavigator } from 'react-navigation';
import { MaterialCommunityIcons, Entypo, Feather, MaterialIcons, Foundation } from '@expo/vector-icons';
import React from 'react';

// SCREENS
import RoutesHome from '../../screens/home/routes-home';
import RoutesSearch from '../../screens/search/routes-search';
import Add from '../../screens/add/Add';
import RoutesFollow from '../../screens/follow/routes-follow';
import Profile from '../../screens/profile/Profile';


const RoutingAuthenticated = TabNavigator(
  {
    Home: {
      screen: RoutesHome,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          focused ? <Entypo name="home" size={30} color="#282828" /> : <Entypo name="home" size={30} color="#c1c1c1" />
        ),
      },
    },
    Search: {
      screen: RoutesSearch,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          focused ? <Feather name="search" size={30} color="#282828" /> : <Feather name="search" size={30} color="#c1c1c1" />
        ),
      },
    },
    Add: {
      screen: Add,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          focused ? <MaterialIcons name="add-box" size={30} color="#282828" /> : <MaterialIcons name="add-box" size={30} color="#c1c1c1" />
        ),
      },
    },
    Follow: {
      screen: RoutesFollow,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          focused ? <Foundation name="heart" size={30} color="#282828" /> : <Foundation name="heart" size={30} color="#c1c1c1" />
        ),
      },
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          focused ? <MaterialCommunityIcons name="account" size={30} color="#282828" /> : <MaterialCommunityIcons name="account" size={30} color="#c1c1c1" />
        ),
      },
    },
  },
  {
    tabBarPosition: 'bottom',
    tabBarOptions: {
      showLabel: false,
      showIcon: true,
    },
  },
);


export default RoutingAuthenticated;
