// NATIVE
import { StackNavigator } from 'react-navigation';

// SCREENS
import SignIn from '../../screens/signin/Signin';
import SignUp from '../../screens/signup/Signup';

const RoutingNoAuthenticated = StackNavigator(
  {
    SignIn: {
      screen: SignIn,
      navigationOptions: {
        header: null,
      },
    },
    SignUp: {
      screen: SignUp,
    },
  },
  {
    initialRouteName: 'SignIn', // Desde donde comienza el app
    navigationOptions: { // Configuracion general para screens

    },
  },
);


export default RoutingNoAuthenticated;
