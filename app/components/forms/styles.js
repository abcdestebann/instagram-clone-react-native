import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '80%',
    justifyContent: 'center',
  },
  input: {
    borderWidth: 1,
    borderColor: '#efefef',
    borderRadius: 3,
    padding: 10,
    marginTop: 10,
    width: '100%',
    backgroundColor: '#fafafa',
  },
  button: {
    backgroundColor: '#3897f0',
    borderColor: '#3897f0',
    marginTop: 15,
    padding: 10,
    borderRadius: 5,
  },
  textButton: {
    color: '#fff',
    alignSelf: 'center',
    fontWeight: 'bold',
    fontSize: 16,
  },
  textError: {
    fontSize: 12,
    color: '#f96666',
  },
});

export default styles;
