import React, { Component } from 'react';
import { Text, View, TextInput, Button, TouchableOpacity } from 'react-native';
import { Field, reduxForm } from 'redux-form';

import Input from '../input/input';

import styles from './styles';

const SignInForm = (props) => {
  console.log(props);
  return (
    <View style={styles.container}>
      <Field name="email" component={Input} placeholder="Correo electrónico" />
      <Field name="password" component={Input} placeholder="Contraseña" />
      <TouchableOpacity style={styles.button} onPress={props.handleSubmit((values) => { (props.valid) ? console.log(values) : {}; })} underlayColor="#fff">
        <Text style={styles.textButton} >Iniciar sesión</Text>
      </TouchableOpacity>
    </View>
  );
};

const validate = (values) => {
  const errors = {};

  errors.email = validateEmail(values);
  errors.password = validatePassword(values);

  return errors;
};

const validateEmail = (values) => {
  const email = !values.email
    ? 'El correo electrónico es requerido'
    : !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
      ? 'Formato inválido'
      : undefined;
  return email;
};

const validatePassword = (values) => {
  const password = !values.password
    ? 'La contraseña es requerida'
    : undefined;

  return password;
};


export default reduxForm({ form: 'SignInForm', validate })(SignInForm);
