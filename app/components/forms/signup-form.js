import React, { Component } from 'react';
import { Text, View, TextInput, Button, TouchableOpacity } from 'react-native';
import { Field, reduxForm } from 'redux-form';

import Input from '../input/input';

import styles from './styles';

const SignUpForm = (props) => {
  console.log(props);
  return (
    <View style={styles.container}>
      <Field name="name" component={Input} placeholder="Nombre de usuario" />
      <Field name="email" component={Input} placeholder="Correo electrónico" />
      <Field name="password" component={Input} placeholder="Contraseña" />
      <Field name="confirm" component={Input} placeholder="Confirmar contraseña" />
      <TouchableOpacity style={styles.button} onPress={props.handleSubmit((values) => { (props.valid) ? console.log(values) : {}; })} underlayColor="#fff">
        <Text style={styles.textButton} >Registrarte</Text>
      </TouchableOpacity>
    </View>
  );
};

const validate = (values) => {
  const errors = {};

  errors.name = validateName(values);
  errors.email = validateEmail(values);
  errors.password = validatePassword(values);
  errors.confirm = valifatePasswordConfirm(values);

  return errors;
};

const validateName = (values) => {
  const name = !values.name
    ? 'El nombre de usuario es requerido'
    : values.name.length < 5
      ? 'Debe ser mayor a 5 caracteres'
      : values.name.length > 20
        ? 'Debe ser menor a 20 caracteres'
        : undefined;
  return name;
};

const validateEmail = (values) => {
  const email = !values.email
    ? 'El correo electrónico es requerido'
    : !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
      ? 'Formato inválido'
      : undefined;
  return email;
};

const validatePassword = (values) => {
  const password = !values.password
    ? 'La contraseña es requerida'
    : values.password.length < 5
      ? 'Debe ser mayor a 5 caracteres'
      : values.password.length > 15
        ? 'Debe ser menor a 15 caracteres'
        : undefined;

  return password;
};

const valifatePasswordConfirm = (values) => {
  const confirm = !values.confirm
    ? 'Confirmar contraseña es requerido'
    : values.confirm !== values.password
      ? 'Las contraseñas no coinciden'
      : undefined;

  return confirm;
};

export default reduxForm({ form: 'SignUpForm', validate })(SignUpForm);
