import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';

import styles from '../forms/styles';

const Input = (props) => {
  console.log(props);
  const { input, meta } = props;
  return (
    <View>
      <TextInput
        placeholder={props.placeholder}
        onChangeText={input.onChange}
        value={input.value}
        keyboardType={input.name === 'email' ? 'email-address' : 'default'}
        secureTextEntry={!!((input.name == 'password' || input.name == 'confirm'))}
        autoCapitalize="none"
        onBlur={input.onBlur}
        style={styles.input}
      />
      { meta.touched && meta.error && <Text style={styles.textError} > {meta.error} </Text> }
    </View>
  );
};

export default Input;
