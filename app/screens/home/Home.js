import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';

import globalStyles from '../../config/styles';

export default class Home extends Component {
  render() {
    const { navigation } = this.props;
    console.log(navigation);
    return (
      <View style={globalStyles.container} >
        <Text> Home </Text>
        <Button title="User" onPress={() => { navigation.push('User'); }} />
        <Button title="Comments" onPress={() => { navigation.push('Comments'); }} />
      </View >
    );
  }
}
