import { StackNavigator } from 'react-navigation';

import Home from './Home';
import Profile from '../profile/Profile';
import Post from '../post/Post';
import Comments from '../comments/Comments';

const RoutesHome = StackNavigator({
  Home: {
    screen: Home,
  },
  User: {
    screen: Profile,
  },
  Post: {
    screen: Post,
  },
  Comments: {
    screen: Comments,
    // navigationOptions: {
    //   tabBarVisible: false, // Hereda del navigator principal que es Tab Navigator
    // },
  },
});

export default RoutesHome;
