import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';

import globalStyles from '../../config/styles';

export default class Post extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <View style={globalStyles.container}>
        <Text> Post </Text>
        <Button title="Comments" onPress={() => { navigation.push('Comments'); }} />
        <Button title="User" onPress={() => { navigation.push('User'); }} />
      </View>
    );
  }
}
