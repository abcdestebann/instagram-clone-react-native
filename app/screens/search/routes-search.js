import { StackNavigator } from 'react-navigation';

import Search from './Search';
import Profile from '../profile/Profile';
import Post from '../post/Post';
import Comments from '../comments/Comments';

const RoutesSearch = StackNavigator({
  Search: {
    screen: Search,
  },
  Post: {
    screen: Post,
  },
  User: {
    screen: Profile,
  },
  Comments: {
    screen: Comments,
    navigationOptions: {
      tabBarVisible: false, // Hereda del navigator principal que es Tab Navigator
    },
  },
});

export default RoutesSearch;
