import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';

import globalStyles from '../../config/styles';

export default class Profile extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <View style={globalStyles.container}>
        <Text> Profile </Text>
        <Button title="Post" onPress={() => navigation.push('Post')} />
      </View>
    );
  }
}
