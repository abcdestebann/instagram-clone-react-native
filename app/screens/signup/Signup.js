import React, { Component } from 'react';
import { View, Text, Button, TextInput } from 'react-native';
import styles from './styles';

import SignUpForm from '../../components/forms/signup-form';

export default class SignUp extends Component {
  constructor() {
    super();
    this.state = {};
  }
  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <SignUpForm />
      </View>
    );
  }
}

