import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';

import globalStyles from '../../config/styles';

export default class Follow extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <View style={globalStyles.container}>
        <Text> Follow </Text>
        <Button title="User" onPress={() => navigation.push('User')} />
      </View>
    );
  }
}
