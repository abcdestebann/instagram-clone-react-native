import { TabNavigator, StackNavigator } from 'react-navigation';
import Follow from './Follow';
import Profile from '../profile/Profile';
import Post from '../post/Post';
import Comments from '../comments/Comments';

const TabsFollow = TabNavigator({
  Follow: {
    screen: Follow,
  },
  Followers: {
    screen: Follow,
  },
}, {
  tabBarPosition: 'top',
  swipeEnabled: true,
  tabBarOptions: {
    activeTintColor: 'black',
  },
});

const RoutesFollow = StackNavigator({
  TabFollow: {
    screen: TabsFollow,
    navigationOptions: {
      header: null,
    },
  },
  User: {
    screen: Profile,
  },
  Post: {
    screen: Post,
  },
  Comments: {
    screen: Comments,
    // navigationOptions: {
    //   tabBarVisible: false, // Hereda del navigator principal que es Tab Navigator
    // },
  },
});

export default RoutesFollow;
