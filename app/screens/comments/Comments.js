import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';

import globalStyles from '../../config/styles';

export default class Comments extends Component {
  static navigationOptions = {
    tabBarVisible: false, // Hereda del navigator principal que es Tab Navigator
  }

  render() {
    console.log(this.props);
    const { navigation } = this.props;
    return (
      <View style={globalStyles.container}>
        <Text> Comments </Text>
        <Button title="Autor" onPress={() => { navigation.push('User'); }} />
      </View>
    );
  }
}
