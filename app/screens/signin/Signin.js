import React, { Component } from 'react';
import { View, Text, Button, TouchableOpacity } from 'react-native';

import styles from './styles';

import { connect } from 'react-redux';

import SignInForm from '../../components/forms/signin-form';


class SignIn extends Component {
  constructor() {
    super();
    this.state = {};
  }
  render() {
    const { navigation } = this.props;
    console.log(this.props);
    return (
      <View style={styles.container} >
        <SignInForm />
        <View style={styles.register}>
          <Text style={styles.textRegister} >¿Eres nuevo en Instagram?</Text>
          <TouchableOpacity onPress={() => { navigation.push('SignUp'); }} underlayColor="#fff">
            <Text style={styles.sizeText}> Regístrate.</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  numero: state.reducer,
});

const mapDispatchToProps = dispatch => ({
  aumentar: () => {
    dispatch({ type: 'AUM' });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);

