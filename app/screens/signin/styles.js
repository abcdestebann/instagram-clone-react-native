import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  register: {
    position: 'absolute',
    bottom: 10,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 10,
    borderTopWidth: 1,
    borderTopColor: '#dddddd',
    width: '100%',
  },
  textRegister: {
    color: '#afafaf',
    fontSize: 12,
  },
  sizeText: {
    fontSize: 12,
  },
});

export default styles;
