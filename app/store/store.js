import { createStore, combineReducers } from 'redux';
import { reducer as form } from 'redux-form';


const reducer = (state = [0], action) => state;

const reducers = combineReducers({
  reducer,
  form,
});

const store = createStore(reducers);


export default store;

